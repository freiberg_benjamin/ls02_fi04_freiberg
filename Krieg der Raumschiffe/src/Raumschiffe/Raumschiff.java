package Raumschiffe;

import java.util.ArrayList;

public class Raumschiff {

	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungInProzent;
	private int androidenAnzahl;
	private String schiffname;
	private static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent, int huelleInProzent, int lebenserhaltungInProzent, int androidenAnzahl, String schiffname, ArrayList<String> broadcastKommunikator, ArrayList<String> ladungsverzeichnis) {
		this.photonentorpedoAnzahl = 0;
		this.energieversorgungInProzent = 0;
		this.schildeInProzent = 0;
		this.huelleInProzent = 0;
		this.lebenserhaltungInProzent = 0;
		this.androidenAnzahl = 0;
		this.schiffname = "Ulyssis";
		
	}
		public int getphotonentorpedoAnzahl() {
			return photonentorpedoAnzahl;
		}
		public void setphotonentorpedoAnzahl(int photonentorpedoAnzahl) {
			if(photonentorpedoAnzahl > 0)
				this.photonentorpedoAnzahl = photonentorpedoAnzahl;
			else
				this.photonentorpedoAnzahl = 0;
		}
		public int getenergieversorgungInProzent() {
			return energieversorgungInProzent;
		}
		public void setenergieversorgungInProzent(int energieversorgungInProzent) {
			if(energieversorgungInProzent > 0)
				this.energieversorgungInProzent = energieversorgungInProzent;
			else
				this.energieversorgungInProzent = 0;
		}
		public int getschildeInProzent() {
			return schildeInProzent;
		}
		public void setschildeInProzent(int schildeInProzent) {
			if(schildeInProzent > 0)
				this.schildeInProzent = schildeInProzent;
			else
				this.schildeInProzent = 0;
		}
		public int gethuelleInProzent() {
			return huelleInProzent;
		}
		public void sethuelleInProzent(int huelleInProzent) {
			if(huelleInProzent > 0)
				this.huelleInProzent = huelleInProzent;
			else
				this.huelleInProzent = 0;
		}
		public int getlebenserhaltungInProzent() {
			return lebenserhaltungInProzent;
		}
		public void setlebenserhaltungInProzent(int lebenserhaltungInProzent) {
			if(lebenserhaltungInProzent > 0)
				this.lebenserhaltungInProzent = lebenserhaltungInProzent;
			else
				this.lebenserhaltungInProzent = 0;
		}
		public int getandroidenAnzahl() {
			return androidenAnzahl;
		}
		public void setandroidenAnzahl(int androidenAnzahl) {
			if(androidenAnzahl > 0)
				this.androidenAnzahl = androidenAnzahl;
			else
				this.androidenAnzahl = 0;
		}
		public String getschiffname() {
			return schiffname;
		}
		public void setschiffname(String schiffname) {
			if(schiffname == "")
				this.schiffname = "schiffname";
			else
				this.schiffname = "unbekanntes Schiff";
		}
		public ArrayList<String> getbroadcastKommunikator(){
			return broadcastKommunikator;
		}
		public static void setbroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
				Raumschiff.broadcastKommunikator = broadcastKommunikator;
		}
		public ArrayList<Ladung> getladungsverzeichnis(){
			return ladungsverzeichnis;
		}
		public void setladungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
			this.ladungsverzeichnis = ladungsverzeichnis;
		}
		public void addLadung(Ladung neueLadung) {
			this.ladungsverzeichnis.add(neueLadung);
		}
		public void ladungsverzeichnisAusgeben() {
			System.out.println("\nLadungsverzeichnis:");
			this.ladungsverzeichnis.forEach(ladung ->{
				System.out.printf("Bezeichnung: %s / Menge: %d\n",ladung.getBezeichnung(), ladung.getMenge());
				});		
		}
		public void nachrichtAnAlle(String message) {
			Raumschiff.broadcastKommunikator.add(message);
			System.out.println("\n" + message);
		}
		public void phaserkanoneSchießen(Raumschiff r) {
			if(this.energieversorgungInProzent > 50) {
				this.energieversorgungInProzent = this.energieversorgungInProzent - 50;
				// nachricht an alle
				// treffer aufrufen
			}
			else {
				//nachricht an alle
				//click
			}
		}
}